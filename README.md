This project is for the organization of other holoProjects :)


our priorities are listed [here](Priorities.md)

## work in progress

* framaPads:
   * [holoNotes](https://mensuel.framapad.org/p/holoNotes)
   * [holoActions](https://mensuel.framapad.org/p/holoActions)
   * [holoFridays](https://bimestriel.framapad.org/p/holoFridays)
   * [holoPad](https://bimestriel.framapad.org/p/holoPad)

   * [holoGIT](https://mensuel.framapad.org/p/holoGIT)
   * [michel-upgrade](https://mensuel.framapad.org/p/michel-upgrade)

   * [holoWhiteBoard](https://mensuel.framapad.org/p/holoWhiteBoard)
   * [holoNotes](https://bimestriel.framapad.org/p/holoNotes)

   * [krysthalWhiteBoard](https://annuel2.framapad.org/p/4skrysthalWhiteBoard)
   * [IRP methodology](https://annuel2.framapad.org/p/IRP)
* frama-pages:
   * [holosphere](https://holosphere.frama.io/)
   * [holoBin](https://holosphere.frama.io/holotools/holobin/) (encrypted)

## important documents :

* HoloWhitePapers : https://app.clickup.com/2497563/v/dc/2c70v-477/2c70v-106
* HoloSphere OS : https://docs.google.com/document/d/1e_feUAG1X4H9mtysAI8JRYw6dpQiSWnLSoaoPe1fyhs/edit?usp=sharing
* other clickup docs : https://app.clickup.com/2497563/docs





our indexes are :

* [holoGitRegistry](holoGitRegostry.csv)
* [holoPadRegistry](holoPadRegistry.csv)
* [holoSiteRegistry](holoSiteRegistry.csv)

### Repositories structure :

We have organized our GIT repository in [groups][1] as followed
([gitlab][2] for "public-facing" repositories and [framagit][3] for more "private" ones)

* Git Repositories Index ([clickup](https://app.clickup.com/2497563/v/dc/2c70v-491/2c70v-120))

#### gitlab.com (side)

#####  [holosphere4](https://gitlab.com/holosphere4)
 - holoVersity
    - [onboarding](https://gitlab.com/holosphere4/holoversity/onboarding)
   
#####  [kin4](https://gitlab.com/kin4)
 - [MLP dev](https://gitlab.com/groups/kin4/MLP/)
 - holoWiki
 - [holoProfile](https://gitlab.com/kin4/holoprofile)
 - [holoBook](https://gitlab.com/kin4/holobook)
 - [holoLabs](https://gitlab.com/kin4/hololabs) (experiment and holotools "incubators")
     - [holoLinks](https://gitlab.com/kin4/hololabs/hololinks)
     - [holoIndexes](https://gitlab.com/kin4/hololabs/holoindexes)

#### framagit.org (side)

* [holoTeam](https://framagit.org/holoteam)
    - [holoNotes](https://framagit.org/holotools/holonotes)
    - [holoPass](https://framagit.org/holotools/holopass)
* [holoTools](https://framagit.org/holotools/)
    - [holoBin](https://framagit.org/holosphere/holotools/holobin) (encrypted pad)
    - [holoDNS](https://framagit.org/holosphere/holotools/holoDNS) (self hosted DNS & LNS)
* [kin](https://framagit.org/kin/)
    - [holoLedger](https://framagit.org/kin/hololedger) (private)
    - [holoLicense](https://framagit.org/kin/hololicense) [KHL]
    - [holoSites](https://framagit.org/kin/holosites)
    - [holowiki](https://framagit.org/kin/holowiki) (private backup)

[1]: https://gitlab.com/dashboard/groups
[2]: https://gitlab.com/dashboard/projects
[3]: https://framagit.org/dashboard/projects
