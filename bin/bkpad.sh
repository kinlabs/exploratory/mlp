#

echo "--- ${0##*/}"
echo zero: $0
IPFS_PATH=/media/IPFS/HOLORING

git fetch
gittop=$(git rev-parse --show-toplevel)
PATH=$gittop/bin:$PATH


ls -l $gittop/*PadReg*;
reg=$(ls -1  $gittop/*PadReg*.sul)
for url in $(tail +2 $reg); do
 url="${url%% *}"
 url=${url%\?*}
 padid=${url##*/}
 padid=${padid%.md}
 echo pad: $padid
 echo url: $url
 qm=$(ipfs add -Q $url/export/html)
 curl -s -o Pads/$padid.json $url/export/etherpad
 curl -s -o Pads/$padid.txt $url/export/txt
 curl -s -o Pads/$padid.md $url/export/markdown
 curl -s -o Pads/$padid.htm $url/export/html
 curl -s -o Pads/$padid.pdf $url/export/pdf
 perl -S json2yml.pl Pads/$padid.json > Pads/$padid.yml
 qm=$(ipfs add -Q -w Pads/$padid.*)
 echo $padid: $qm >> pads.log
 eval $(perl -S listpadrev.pl Pads/$padid.yml | eyml)
 echo $padid: $mtic $head $url $expires $ttl $xdate $qm | tee -a _data/pads.yml
done
