#!/usr/bin/perl


my $yml;
my $url = shift;

printf "--- # %s\n",$0;
use JSON qw(decode_json);
use YAML::Syck qw(LoadFile DumpFile Dump);

my ($fpath,$fname,$padid);
if ($url =~ m/^https?:/) {
   printf "url: %s\n",$url;
   $url =~ s/\?.*//;
   ($fpath,$padid,$bname,undef) = &fname($url);
   use LWP::Simple qw(get);
   my $content = get $url.'/export/etherpad';
   die "Couldn't get $url" unless defined $content;
   $yml = &decode_json($content);
   DumpFile("Pads/$bname.yml",$yml);
} elsif (-e $url) {
  ($fpath,$fname,$padid,undef) = &fname($url);
   printf "file: %s\n",$url;
   $yml = LoadFile($url);
}
printf "padid: %s\n",$padid;


my $key = 'pad:'.$padid;

my $first = $key.':revs:0';
my $warn = $yml->{$first}{meta}{atext}{text};
#printf "warn: %s\n",$warn;
my $expires = $1 if ($warn =~ m/(\d+)\s*jours/i);
printf "expires: %d\n",$expires;
if ($expires eq '365') {
  $url = 'https://annuel2.framapad.org/p/'.$padid;
} elsif ($expires eq '63') {
  $url = 'https://bimestriel.framapad.org/p/'.$padid
} elsif ($expires eq '31') {
  $url = 'https://mensuel.framapad.org/p/'.$padid
}
printf "url: %s\n",$url;

my $text = $yml->{$key}{atext}{text};
my $p = index($text,"\n");
my $line1 =  substr($text,0,($p<0)?78:$p);
$yml->{$key}{text78} = $line1 . '...';
delete $yml->{$key}{atext};
delete $yml->{$key}{pool}{numToAttrib};

printf "pad: %s\n",Dump($yml->{$key});

my $head = $yml->{$key}{head};
printf "head: %s\n",$head;



my $last = $key.':revs:'.$head;
printf "last: %s\n",$last;
printf "meta: %s\n",Dump($yml->{$last}{meta});

my $mtic = $yml->{$last}{meta}{timestamp} / 1000;
printf "mtic: %s\n",$mtic;
my $mdate = &sdate($mtic);
printf "mdate: %s\n",$mdate;
my $id = $yml->{$last}{meta}{author};
my $author = $yml->{'globalAuthor:'.$id}{name};
printf "author: %s\n",$author || $id;
my $xtic = $mtic + $expires * 24 * 3600;
my $xdate = &sdate($xtic);
printf "xdate: %s\n",$xdate;
my $ttl = $xtic - $^T;
printf "ttl: %.1f\n",$ttl / 24 / 3600;




exit $?;

sub sdate { # return a human readable date ... but still sortable ...
  my $tic = int ($_[0]);
  my $ms = ($_[0] - $tic) * 1000;
     $ms = ($ms) ? sprintf('%04u',$ms) : '____';
  my ($sec,$min,$hour,$mday,$mon,$yy) = (localtime($tic))[0..5];
  my ($yr4,$yr2) =($yy+1900,$yy%100);
  my $date = sprintf '%04u-%02u-%02u %02u.%02u.%02u',
             $yr4,$mon+1,$mday, $hour,$min,$sec;
  return $date;
}


sub fname { # extract filename etc...
  my $f = shift;
  $f =~ s,\\,/,g; # *nix style !
  my $s = rindex($f,'/');
  my $fpath = '.';
  if ($s > 0) {
    $fpath = substr($f,0,$s);
  } else {
    use Cwd;
    $fpath = Cwd::getcwd();
  }
  my $file = substr($f,$s+1);
  if (-d $f) {
    return ($fpath,$file);
  } else {
  my $bname;
  my $p = rindex($file,'.');
  my $ext = lc substr($file,$p+1);
     $ext =~ s/\~$//;
  if ($p > 0) {
    $bname = substr($file,0,$p);
    $ext = lc substr($file,$p+1);
    $ext =~ s/\~$//;
  } else {
    $bname = $file;
    $ext = 'dat';
  }
  $bname =~ s/\s+\(\d+\)$//; # remove (1) in names ...

  return ($fpath,$file,$bname,$ext);

  }
}


1; # $Source: /my/perl/scritpts/listpadrev.pl $
